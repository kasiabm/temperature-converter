package temperatureConverter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

/**
 * Simple class converting temperature readings between Celsius, Fahrenheit 
 * and Kelvin scales.
 * @author KasiaBM
 * @version Jan 9, 2015
 */

public class TemperatureConverter extends JFrame {
    //instance variables
    private final String[] scales = {"Celsius", "Fahrenheit", "Kelvin"};
    private JPanel scalesPanel;
    private JPanel readingPanel;
    private JPanel resultPanel;
    private JLabel instructionLabel1;
    private JLabel instructionLabel2;
    private JLabel fromLabel;
    private JLabel toLabel;
    private JLabel resultLabel;
    private JComboBox fromScaleMenu;
    private JComboBox toScaleMenu;
    private JTextField inputField;
    private JButton convertButton;
    private int fromScale;
    private int toScale;
    private double result;
    
    //constructor
    public TemperatureConverter() {
        //sets up the frame
        super("Temperature Converter");
        setLayout(new FlowLayout(FlowLayout.LEFT));
        setSize(270, 240);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //sets up the components
        scalesPanel = new JPanel(new GridLayout(2, 2));
        readingPanel = new JPanel(new GridLayout(1, 2));
        resultPanel = new JPanel();
        instructionLabel1 = new JLabel("<html>From the dropdown menus below<br>"
                + "select the scales for your conversion</html>");
        instructionLabel2 = new JLabel("<html>Enter your temperature reading<br>"
                + "and click \"Convert\"</html>");
        fromLabel = new JLabel("From:");
        toLabel = new JLabel("To:");
        resultLabel = new JLabel("Result: ");
        fromScaleMenu = new JComboBox(scales);
        fromScaleMenu.setEditable(false);
        toScaleMenu = new JComboBox(scales);
        toScaleMenu.setEditable(false);
        inputField = new JTextField(9);
        convertButton = new JButton("Convert");
        fromScale = 0;
        toScale = 0;
        result = 0.0;
        
        //adds components to panels
        scalesPanel.add(fromLabel);
        scalesPanel.add(toLabel);
        scalesPanel.add(fromScaleMenu);
        scalesPanel.add(toScaleMenu);
        readingPanel.add(inputField);
        readingPanel.add(convertButton);
        resultPanel.add(resultLabel);
        add(instructionLabel1);
        add(scalesPanel);
        add(instructionLabel2);
        add(readingPanel);
        add(resultPanel);
        
        //registers event handlers
        fromScaleMenu.addItemListener(
            new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        fromScale = fromScaleMenu.getSelectedIndex();
                    }
                } //end method itemStateChanged
            } //end class ItemListener
        );

        toScaleMenu.addItemListener(
        new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    toScale = toScaleMenu.getSelectedIndex();
                    }
                } //end method itemStateChanged
            } //end class ItemListener
        );
        
        convertButton.addActionListener(new ConvertButtonHandler());
        
        //sets the window to visible
        setVisible(true);
    } //end constructor TemperatureConverter
    
    //anonymous class handling the convert button event, calculating
    //displaying result
    private class ConvertButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ae) {
            String inputText = inputField.getText();
            if (inputText.isEmpty()) {
                resultLabel.setText("<html>Enter temperature value to convert<br>"
                        + "and click \"Convert\"</html>");
            }
            else {
                try {
                    double fromTemp = Double.parseDouble(inputText);
                    if (fromScale == toScale) {
                        resultLabel.setText("<html>Select different scale to "
                                + "convert to<br>and click \"Convert\"</html>");
                    }
                    else {
                        if (fromScale == 0 && toScale == 1) {
                            result = 9.0 / 5.0 * fromTemp + 32;
                        }
                        if (fromScale == 0 && toScale == 2) {
                           result = fromTemp + 273.15;
                        }
                        if (fromScale == 1 && toScale == 0) {
                            result = 5.0 / 9.0 * (fromTemp - 32);
                        }
                        if (fromScale == 1 && toScale == 2) {
                            result = 5.0 / 9.0 * (fromTemp - 32) + 273.15;
                        }
                        if (fromScale == 2 && toScale == 0) {
                            result = fromTemp - 273.15;
                        }
                        if (fromScale == 2 && toScale == 1) {
                            result = fromTemp * 9.0 / 5.0 - 459.67;
                        }
                        resultLabel.setText(String.format("Result: %.2f\u00B0",
                            result)); 
                    }
                } //end try
                catch (NumberFormatException nfe) {
                    inputField.setText("");
                    resultLabel.setText("<html>Enter valid temperature value "
                            + "to convert<br>and click \"Convert\"</html>");
                } //end catch
            } //end else
        } //end method actionPerformed
    } //end class ConvertButtonHandler
} //end class TemperatureConverter